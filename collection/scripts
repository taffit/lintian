#!/usr/bin/perl -w
# scripts -- lintian collection script

# Copyright © 1998 Richard Braakman
# Copyright © 2019 Felix Lechner
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::coll::scripts;

no lib '.';

use strict;
use warnings;
use autodie;

use BerkeleyDB;
use MLDBM qw(BerkeleyDB::Btree Storable);
use Path::Tiny;

use lib "$ENV{'LINTIAN_ROOT'}/lib/";

use Lintian::Collect;

use constant EMPTY => q{};
use constant SPACE => q{ };
use constant NEWLINE => qq{\n};

sub collect {
    my ($pkg, $type, $dir) = @_;

    # any scripts shipped in the package
    my $script_dbpath = "$dir/scripts.db";
    unlink $script_dbpath
      if -e $script_dbpath;

    tie my %plain, 'MLDBM',
      -Filename => $script_dbpath,
      -Flags    => DB_CREATE
      or die "Cannot open file $script_dbpath: $! $BerkeleyDB::Error\n";

    my $info = Lintian::Collect->new($pkg, $type, $dir);
    foreach my $path ($info->sorted_index) {
        next unless $path->is_regular_file and $path->is_open_ok;

        # skip lincity data files; magic: #!#!#!
        next if $path->magic(6) eq '#!#!#!';

        # no shebang => no script
        my $interpreter = $path->get_interpreter;
        next
          unless defined $interpreter;

        # remove comment, if any
        my ($stripped) = ($interpreter =~ /^([^#]*)/);

        my %record;

        # remove /usr/bin/env
        $record{calls_env} = 1
          if $stripped =~ s,^/usr/bin/env\s+,,;

        # get base command without options
        $stripped =~ s/\s++ .++ \Z//xsm;

        $record{interpreter} = $stripped || $interpreter;

        $plain{$path} = \%record;
    }

    untie %plain;

    # maintainer scripts
    my $control_dbpath = "$dir/control-scripts.db";
    unlink $control_dbpath
      if -e $control_dbpath;

    tie my %control, 'BerkeleyDB::Btree',
      -Filename => $control_dbpath,
      -Flags    => DB_CREATE
      or die "Cannot open file $control_dbpath: $! $BerkeleyDB::Error\n";

    for my $path ($info->control_index(EMPTY)->children) {
        next unless $path->is_open_ok;

        # skip anything other than maintainer scripts
        next unless $path =~ m/^(?:(?:pre|post)(?:inst|rm)|config)$/;

        # allow elf binary
        if ($path->magic(4) eq "\x7FELF") {
            $control{$path} = 'ELF';
            next;
        }

        # check for hashbang
        my $interpreter = $path->get_interpreter // EMPTY;

        # get base command without options
        $interpreter =~ s/\s++ .++ \Z//xsm;

        $control{$path} = $interpreter;
    }

    untie %control;

    return;
}

collect(@ARGV) unless caller;

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
